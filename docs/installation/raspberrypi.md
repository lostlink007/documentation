# Raspberry Pi

This version is available in all the variants offered by Parrot: [Core, Home and Security editions](https://parrotsec.org/download/?version=raspberry).

## Installation process

To proceed with the installation, you will need to get a microSD card of at least 8 GB (the Core edition however can also be installed on a 4 GB microSD).

:::info Note
  This procedure applies to any edition of Parrot on Raspberry Pi. Currently ParrotOS has been successfully tested on a Raspberry Pi 3B, 4B, 400, and 5.
:::


Now, download the ParrotOS edition of your choice from our [website](https://parrotsec.org).

Then, insert the micro sd into your computer, and in the meantime, download the Raspberry Pi Imager or Balena Etcher. We will need one of these two to install the system in the microSD.

![imager](./images/rpi/1.png)

Click on *Choose OS* and select **Use custom**.

![imager](./images/rpi/2.png)

Now a window will open where you can select the downloaded ParrotOS edition. It is a compressed **img.xz** file.

Then select your micro sd by clicking on *Choose Storage*.

Everything is ready, click on *Write* and the writing procedure on the micro sd will start. Once finished, you can insert your microSD into your Raspberry Pi. Enjoy!

For any questions and/or problems, we kindly ask you to contact us through [our social channels](https://parrotsec.org/community/).